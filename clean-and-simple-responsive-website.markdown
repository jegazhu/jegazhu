Clean and simple responsive website
-----------------------------------


A [Pen](https://codepen.io/jeffbredenkamp/pen/ZpGOAW) by [Jeff Bredenkamp](https://codepen.io/jeffbredenkamp) on [CodePen](https://codepen.io).

[License](https://codepen.io/jeffbredenkamp/pen/ZpGOAW/license).